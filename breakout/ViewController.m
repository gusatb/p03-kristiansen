//
//  ViewController.m
//  breakout
//
//  Created by Patrick Madden on 2/9/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize gameScreen;

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	[gameScreen createPlayField];
    
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end

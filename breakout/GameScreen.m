//
//  GameScreen.m
//  breakout
//
//  Created by Patrick Madden on 2/9/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "GameScreen.h"

@implementation GameScreen
@synthesize bricks;
@synthesize paddle, ball;
@synthesize timer;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

CGSize screenSize;
int bricksRem;

-(void) restart {
    bricksRem = 40;
    for(int j = 0; j < 40; j++){
        [bricks[j] setHidden:NO];
    }
    CGPoint p;
    p.x = 100;
    p.y = 200;
    [ball setCenter:p];
    CGPoint p2;
    p2.x = 200;
    p2.y = screenSize.height - 100;
    [paddle setCenter:p2];
    dx = 2;
    dy = 2;
}

-(void)createPlayField
{
    bricksRem = 40;
    
    screenSize = [[UIScreen mainScreen] bounds].size;
    self.bricks = [[NSMutableArray alloc] init];
    
    int brickWidth = screenSize.width / 10 - 4;
    int brickHeight = 20;
    
    for(int i = 0; i < 40; i++)
    {
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(i%10 * (brickWidth + 2), i/10 * (brickHeight + 2), brickWidth, brickHeight)];
        [self.bricks addObject:v];
        [self addSubview:v];
        switch(i/10){
            case 0: [v setBackgroundColor:[UIColor redColor]];break;
            case 1: [v setBackgroundColor:[UIColor yellowColor]];break;
            case 2: [v setBackgroundColor:[UIColor greenColor]];break;
            case 3: [v setBackgroundColor:[UIColor blueColor]];break;
        }
    }
    
	paddle = [[UIView alloc] initWithFrame:CGRectMake(200, 40, 60, 10)];
	[self addSubview:paddle];
	[paddle setBackgroundColor:[UIColor blackColor]];
	
	ball = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 10, 10)];
	[self addSubview:ball];
	[ball setBackgroundColor:[UIColor redColor]];
	
    
    
    [self restart];
    timer = [NSTimer scheduledTimerWithTimeInterval:.01	target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
	for (UITouch *t in touches)
	{
		CGPoint p = [t locationInView:self];
        CGPoint padd = [paddle center];
        padd.x = p.x;
        [paddle setCenter:padd];
        
	}
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
	[self touchesBegan:touches withEvent:event];
}

-(IBAction)startAnimation:(id)sender
{
	timer = [NSTimer scheduledTimerWithTimeInterval:.01	target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];

}

-(IBAction)stopAnimation:(id)sender
{
	[timer invalidate];
}

-(void)timerEvent:(id)sender
{
	CGRect bounds = [self bounds];
	
	// NSLog(@"Timer event.");
	CGPoint p = [ball center];

	if ((p.x + dx) < 0)
		dx = -dx;
	
	if ((p.y + dy) < 0)
		dy = -dy;
	
	if ((p.x + dx) > bounds.size.width)
		dx = -dx;
	
    if ((p.y + dy) > bounds.size.height){ // LOSER
        [NSThread sleepForTimeInterval:2.0];
        [self restart];
        return;
    }
	
	p.x += dx;
	p.y += dy;
	[ball setCenter:p];
	
	// Now check to see if we intersect with paddle.  If the movement
	// has placed the ball inside the paddle, we reverse that motion
	// in the Y direction.
	if (CGRectIntersectsRect([ball frame], [paddle frame]))
	{
		dy = -dy;
		p.y += 2*dy;
		[ball setCenter:p];
	}
    
    for(int j = 0; j < 40; j++){
        if(![bricks[j] isHidden] && CGRectIntersectsRect([ball frame], [bricks[j] frame])){
            [bricks[j] setHidden:YES];
            dy = -dy;
            p.y += 2*dy;
            [ball setCenter:p];
            bricksRem--;
        }
    }
    
    if(bricksRem == 0){
        [NSThread sleepForTimeInterval:2.0];
        [self restart];
    }
	

}

@end
